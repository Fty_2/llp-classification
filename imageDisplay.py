import matplotlib.pyplot as plt

TotalEpoch=len(train_log)
plt.figure(1)
x = range(0,TotalEpoch)
y1 = train_log
y2 = val_log
plt.plot(x, y1, label='train_loss')
plt.plot(x, y2, color='red', linewidth=1.0, linestyle='--', label='validation_loss')
plt.legend(loc = 'upper right')
plt.xlabel("epoch num")
plt.ylabel("loss")

plt.figure(2)
x = range(0,TotalEpoch)
y1 = train_acy_log
y2 = val_acy_log
plt.plot(x, y1, label='train_accuracy')
plt.plot(x, y2, color='red', linewidth=1.0, linestyle='--', label='validation_accuracy')
plt.legend(loc = 'lower right')
plt.xlabel("epoch num")
plt.ylabel("accuracy")

# import seaborn as sns
# ax = sns.heatmap(image)