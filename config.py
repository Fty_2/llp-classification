'''
Dataset Configuration
'''
imageFilePrefix = "./data/"
imageFileNum = 6
imageRootFiles = ["sig2jZqq.root", "sig2jZvv.root", "sig4jZqq.root", "sig4jZvv.root", "bkgEEQQ.root", "bkgZH.root"]
lableForFiles = [1, 2, 1, 2, 0, 0]
setSize = [0, 2000, 2250, 2500]    #Train set: size[0]~size[1], Validate set: size[1]~size[2], Test set: size[2]~size[3]


'''
Train configuration
'''
classNum = 3
trainBatchSize = 10
valBatchSize = 10
testBatchSize = 1
epochNum = 10
learningRate = 1e-6
logFileName = "./log/trainningLog"
