'''
initialization
'''
import torch.nn as nn
import torch
import torch.optim as optim
from torch.utils.data import Dataset
import resnet as rn
#import matplotlib.pyplot as plt
import constructDataset as ds
import config
import numpy as np
import os


trainbatch = config.trainBatchSize
valbatch = config.valBatchSize
testbatch = config.testBatchSize

trainset = ds.MyDataset(datasetType="train")
valset = ds.MyDataset(datasetType="validate")
# testset = ds.MyDataset(datasetType="test")

trainloader = torch.utils.data.DataLoader(trainset, batch_size=trainbatch, shuffle=True)
valloader = torch.utils.data.DataLoader(valset, batch_size=valbatch, shuffle=True)
# testloader = torch.utils.data.DataLoader(testset, batch_size=testbatch, shuffle=False)


'''
Train
'''
net = rn.resnet50(nb_ch=3, num_classes=config.classNum)
# net.cuda()
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
_ = net.to(device)
print("Resnet model loaded")
print('Using device:', device)
print()

#Temporary variables
train_log, val_log = [], []
train_acy_log, val_acy_log = [], []
trainClassAcy, valClassAcy = np.zeros((config.classNum, config.epochNum)), np.zeros((config.classNum, config.epochNum))
trainlosstmp, vallosstmp = 0, 0
traincorrect, valcorrect = 0, 0
classCorrect, classTotalNum = np.zeros((config.classNum)), np.zeros((config.classNum))
running_loss = 0.0

if os.path.exists(config.logFileName):
    os.remove(config.logFileName)
logFile = open(config.logFileName, "a")

criterion = nn.CrossEntropyLoss()
optimizer = optim.Adam(net.parameters(), lr=config.learningRate)
epochNum = config.epochNum
for epoch in range(epochNum):

    for i, data in enumerate(trainloader, 0):
        net.train()
        inputs, labels = data
        inputs, labels = inputs.to(device), labels.to(device)
        optimizer.zero_grad()
        outputs = net(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()
        # acc = (predict==labels).sum().item()/trainbatch

    numOfBatches = 0
    for i, data in enumerate(trainloader, 0):
        numOfBatches += 1
        net.eval()
        inputs, labels = data
        inputs, labels = inputs.to(device), labels.to(device)
        outputs = net(inputs)
        loss = criterion(outputs, labels)
        _,predict = torch.max(outputs.detach(),1)
        traincorrect += (predict==labels).sum().item()
        trainlosstmp += float(loss)
        for j in range(config.classNum):
            labelForJ = (labels == j)
            predictForJ = (predict == j)
            classTotalNum[j] += labelForJ.sum().item()
            classCorrect[j] += (labelForJ & predictForJ).sum().item()

    trainlosstmp = trainlosstmp / numOfBatches
    train_log.append(trainlosstmp)
    train_acy_log.append(traincorrect/(trainset.__len__()))

    logFile.write("Epoch" + str(epoch) + "\n")
    logFile.write("  Train set loss: " + str(trainlosstmp) + "\n")
    logFile.write("  Train set accuracy: " + str(traincorrect/(trainset.__len__())) + "\n")
    for j in range(config.classNum):
        trainClassAcy[j][epoch] = classCorrect[j] / classTotalNum[j]
        logFile.write("  Train set class" + str(j) + " zaccuracy: " + str(trainClassAcy[j][epoch]) + "\n")


    print("epoch: ",epoch)
    print("train set loss: ", trainlosstmp)
    print("train accuracy: ",traincorrect/(trainset.__len__()) )
    print()

    numOfBatches = 0
    for i, data in enumerate(valloader, 0):
        numOfBatches += 1
        net.eval()
        inputs, labels = data
        inputs, labels = inputs.to(device), labels.to(device)
        outputs = net(inputs)
        loss = criterion(outputs, labels)
        _,predict = torch.max(outputs.detach(),1)
        valcorrect += (predict==labels).sum().item()
        vallosstmp += float(loss)
        for j in range(config.classNum):
            labelForJ = (labels == j)
            predictForJ = (predict == j)
            classTotalNum[j] += labelForJ.sum().item()
            classCorrect[j] += (labelForJ & predictForJ).sum().item()

    vallosstmp = vallosstmp / numOfBatches
    val_log.append(vallosstmp)
    val_acy_log.append(valcorrect/(valset.__len__()))

    logFile.write("  Validate set loss: " + str(vallosstmp) + "\n")
    logFile.write("  Validate set accuracy: " + str(valcorrect/(valset.__len__())) + "\n")
    for j in range(config.classNum):
        valClassAcy[j][epoch] = classCorrect[j] / classTotalNum[j]
        logFile.write("  Validate set class" + str(j) + " accuracy: " + str(valClassAcy[j][epoch]) + "\n")
    logFile.write("\n\n")

    print("validation loss:",vallosstmp)
    print("validation accuracy: ",valcorrect/(valset.__len__() ) )
    print()
    print("labels: ", labels[0:5])
    print("outputs: ", outputs[0:5, :])
    print()
    print()

    torch.save(net, './model/LLPClassification' + str(epoch) + '.pt')

#    usedEpoch = len(train_log)
#    plt.figure(1)
#    x = range(0, usedEpoch)
#    y1 = train_log
#    y2 = val_log
#    plt.plot(x, y1, color='blue', label='train_loss')
#    plt.plot(x, y2, color='red', linewidth=1.0, linestyle='--', label='validation_loss')
#    if epoch == 0:
#        plt.legend(loc='upper right')
#    plt.xlabel("epoch num")
#    plt.ylabel("loss")
#    plt.savefig("Loss.png")
#
#    plt.figure(2)
#    x = range(0, usedEpoch)
#    y1 = train_acy_log
#    y2 = val_acy_log
#    plt.plot(x, y1, color='blue', label='train_accuracy')
#    plt.plot(x, y2, color='red', linewidth=1.0, linestyle='--', label='validation_accuracy')
#    if epoch == 0:
#        plt.legend(loc='lower right')
#    plt.xlabel("epoch num")
#    plt.ylabel("accuracy")
#    plt.savefig("Accuracy.png")
#
#    for j in range(config.classNum):
#        plt.figure(j+3)
#        x = range(0, usedEpoch)
#        y1 = trainClassAcy[j, :usedEpoch]
#        y2 = valClassAcy[j, :usedEpoch]
#        plt.plot(x, y1, color='blue', label='train_accuracy_class' + str(j))
#        plt.plot(x, y2, color='red', linewidth=1.0, linestyle='--', label='validation_accuracy_class' + str(j))
#        if epoch == 0:
#            plt.legend(loc='lower right')
#        plt.xlabel("epoch num")
#        plt.ylabel("accuracy")
#        plt.savefig("Class" + str(j) + "Accuracy.png")
#

  
  
