import uproot
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import torch
import torch.optim as optim
from torch.utils.data import Dataset
import numpy as np
import config
import sys
import resnet as rn

filePrefix = config.imageFilePrefix
fileNum = config.imageFileNum
files = config.imageRootFiles
lableForFiles = config.lableForFiles
setSize = config.setSize


def readRootFile( fileName, mRange, MRange, label):
    '''
    Read all branches within one function
    '''
    elementNum = MRange - mRange
    file = uproot.open(fileName)
    imageT = file["Image/T"].array(library="np", entry_start=mRange, entry_stop=MRange)
    imageE = file["Image/E"].array(library="np", entry_start=mRange, entry_stop=MRange)
    imageD = file["Image/D"].array(library="np", entry_start=mRange, entry_stop=MRange)
    labels = np.zeros(elementNum) + label
    return imageT.reshape([elementNum, 200, 200]), \
           imageE.reshape([elementNum, 200, 200]), \
           imageD.reshape([elementNum, 200, 200]), \
           labels


def readRootFileBranch( fileName, mRange, MRange, label, branch: 'str'):
    '''
    Read one branch only
    Branch options: T E D
    '''
    elementNum = MRange - mRange
    tree = uproot.open(fileName+":Image")
    image = tree[branch].array(library="np", entry_start=mRange, entry_stop=MRange)
    return image.reshape([elementNum, 200, 200])


class MyDataset(Dataset):
    def __init__(self, datasetType = "train"):
        '''
        datasetType options: train validate test
        '''
        print("Constructing dataset: "+ datasetType)
        self.datasetType = datasetType
        self.typeId = 0 if self.datasetType == "train" else (1 if self.datasetType == "validate" else 2)
        self.setSize = setSize

        self.imageT = np.zeros([1, 200, 200])
        self.imageE = np.zeros([1, 200, 200])
        self.imageD = np.zeros([1, 200, 200])
        self.labels = np.array([0])
        for id in range(fileNum):
            print("Reading file: "+files[id]+". Dataset type: "+self.datasetType)
            # tmpT, tmpE, tmpD, tmplable = readRootFile(filePrefix+files[id], self.setSize[id], self.setSize[id+1], lableForFiles[id])
            tmpT = readRootFileBranch(filePrefix+files[id], self.setSize[self.typeId], self.setSize[self.typeId+1], lableForFiles[id], "T")
            print("Image T loaded")
            tmpE = readRootFileBranch(filePrefix+files[id], self.setSize[self.typeId], self.setSize[self.typeId+1], lableForFiles[id], "E")
            print("Image E loaded")
            tmpD = readRootFileBranch(filePrefix+files[id], self.setSize[self.typeId], self.setSize[self.typeId+1], lableForFiles[id], "D")
            print("Image D loaded")
            tmplable = np.zeros(self.setSize[self.typeId+1] - self.setSize[self.typeId]) + lableForFiles[id]
            self.imageT = np.concatenate((self.imageT, tmpT), 0)
            self.imageE = np.concatenate((self.imageE, tmpE), 0)
            self.imageD = np.concatenate((self.imageD, tmpD), 0)
            self.labels = np.concatenate((self.labels, tmplable), 0)
        self.imageT = torch.Tensor(self.imageT[1:])
        self.imageE = torch.Tensor(self.imageE[1:])
        self.imageD = torch.Tensor(self.imageD[1:])
        self.labels = torch.Tensor(self.labels[1:])
        print("Construction complete")

    def __getitem__(self, index):
        image = torch.cat( (self.imageT[index:index+1], self.imageE[index:index+1], self.imageD[index:index+1]), 0)
        return image, int(self.labels[index])

    def __len__(self):
        return len(self.imageT)


